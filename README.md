# ansible-role-cmk-host-agent

Forked from https://github.com/tribe29/ansible-checkmk, since tribe29 decided to publish three roles in a single repo as full playbook and also seem rather unresponsive in regards to issues or updating the roles or even testing their implementation.

In this repo the three roles are unified, different functionality can be excluded/included by setting certain variables (see default/main.yml).

Extended functionality compared to the tribe29 roles at the time of forking:
- installation of agent and plugins via ansible control node, no https(s) connection from target to cmk-server required

## TODO

### SSH Label

In order to connect to a server per ssh (and not 6556/tcp), I want to assign a label during registration. With [Werk #8845](https://checkmk.com/check_mk-werks.php?werk_id=8845) there was a regression though, that supposedly prevents adding labels through python3, as reported [here](https://forum.checkmk.com/t/check-mk-english-regression-with-werk-8845-w-python3/14528)

Tried figuring out a way in python3, didn't manage to, settled for the rule being on folder-basis for now. With check\_mk 1.7 (and the transition to python3 as per [Werk #11080](https://checkmk.com/check_mk-werks.php?werk_id=11080)), I'm hoping that the functionality will be restored again. 

## LICENSE

As per original ansible-galaxy publishing, GPLv2. 
